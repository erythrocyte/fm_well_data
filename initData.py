# !/usr/bin/env python3
# coding: utf-8

class InitData:
	def __init__(self):
		self.wellfile = ''
		self.stepHour = 0
		self.nz = 0

	def readInitFile(self, s):
		f = open(s, 'r')
		d = f.readlines()

		self.wellfile = d[2]
		self.nz = int(d[6])
		self.stepHour = float(d[4])

		f.close()
