# !/usr/bin/env python3
# coding: utf-8


from dateutil import relativedelta
from dateutil.relativedelta import *
from random import uniform

class DataMaker:
	def __init__(self):
		self.fn_out = ''
		self.fn_inj = ''

	def makeOutFiles(self, wellData, s_path, initData):
		f = open(s_path + self.fn_out, 'w')

		lines = []
		lines.append('#header\n')

		for w in wellData:
			print (w.wellName)
			print (w.startDate)
			print(w.endDate)

			r = relativedelta(w.endDate, w.startDate)
			v1 = r.months * (r.years+1)
			print ('v1 = {0}'.format(v1))



			if (w.wellType == 0):
				sc = self.getStepCount(w.startDate, w.endDate, initData.stepType, initData.step)
				print ('sc count = {0}'.format(sc))
				for k in range (sc):
					s = ''
					dt = w.startDate if (k == 0) else self.getNextDate(dt, initData.stepType, initData.step)
					s += w.wellName + ' ' + str(dt) + ' '
					for n in range(initData.nz):
						doil = uniform(0.1, 10)
						s += str(doil + w.oilStart) + ' '
					s.strip()

					for n in range(initData.nz):
						dwat = uniform(0.1, 10)
						s += str(dwat + w.waterStart) + ' '
					s.strip()
					s += '\n'

					lines.append(s)
			

		print (len(lines))

		f.writelines(lines)

		f.close()

	def getNextDate(self, curDate, stepType, step):
		if (stepType == 0):
			return curDate + datetime.timedelta(hours = step)
		elif(stepType == 1):
			return curDate + datetime.timedelta(days = step)
		elif(stepType == 2):
			return curDate + relativedelta(months=step)
		else:
			return curDate + datetime.relativedelta(months=step)



	def getStepCount(self, date1, date2, stepType, step):
		v = 0
		if (stepType == 0):
			v = abs((date2 - date1).hours)
		elif(stepType == 1):
			v = abs((date2 - date1).days)
		elif(stepType == 2):
			r = relativedelta(date2, date1)
			v = r.months * (r.years+1)
		else:
			r = relativedelta.relativedelta(date2, date1)
			v = r.months * (r.years+1)

		return int(v / step)

