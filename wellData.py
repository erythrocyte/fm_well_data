# !/usr/bin/env python3
# coding: utf-8

import datetime
from datetime import date

class OneData:
	def __init__(self):
		self.wellName = ''
		self.startDate = date.today()
		self.endDate = date.today()
		self.wellType = -1; # 0 - out, 1- inj
		self.oilStart = 0.0
		self.waterStart = 0.0
		self.gasStart = 0.0
		self.injStart = 0.0
		self.approxType = -1 # 0 - linear;
	
	def setValsFromStr(s):
		v = s.split(' ')
		print (v[0])

