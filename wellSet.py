# !/usr/bin/env python3
# coding: utf-8

from wellData import OneData

class WellSet:
	def __init__(self):
		self.wells = []

	def readWellSetFile(self, s):
		f = open(s, 'r')
		d = f.readlines()
		f.close()

		for k in range(1, len(d)):
			od = OneData()
			val = str(d[k]).strip()
			od.setValsFromStr(val)
			self.wells.append(od)


